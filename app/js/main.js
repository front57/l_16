function menu(){
    let burger = document.querySelector('.hamburger');
    let header_menu = document.querySelector('.nav__menu');
    let body = document.querySelector('body');
    burger.addEventListener('click',  () => {
        burger.classList.toggle('active');
        header_menu.classList.toggle('active');
        body.classList.toggle('lock');
    });
}

menu();

function toggleAccordion(){
    const acc = document.getElementsByClassName("accordion__header");
    const panels = document.querySelectorAll('.accordion__body');


    [...acc].forEach((accordion, accIdx) => {
        accordion.addEventListener('click', function(){
            panels.forEach((panel, panelIdx) => {
                if(panelIdx !== accIdx){
                    panel.style.maxHeight = null;
                    panel.previousElementSibling.classList.remove('active');
                }else{
                    panel.style.maxHeight = panel.style.maxHeight ? null : panel.scrollHeight + "px";
                    panel.previousElementSibling.classList.toggle('active');
                }
            })
        });
    });
}

toggleAccordion();

window.addEventListener('click', function(e){
    const dd = document.querySelectorAll('.dropdown');
    if(!e.target.classList.contains('dropdown__input')){
        dd.forEach(item => {
            item.classList.remove('active');
        })
    }else{
        dd.forEach(item => {
            if(e.target !== item.querySelector('.dropdown__input')){
                item.classList.remove('active')
            }
        })
    }
});


function showDropdownList(el){
    el.closest(".dropdown__option").previousElementSibling.value = el.textContent;
}

function activateDropdown(){
    const dd = document.querySelectorAll(".dropdown");
    dd.forEach(elem => {
        elem.addEventListener('click', function(){
            elem.classList.toggle("active");
        });
    });
}

activateDropdown()

function setImageAsBackground() {
    const imgs = document.querySelectorAll('.background-img');

    imgs.forEach(e => {
        const imgSrc = e.src;
        e.parentElement.style.backgroundImage = `url('${imgSrc}')`;
        e.parentElement.classList.add('background-center');
        e.style.display = 'none';
    });
}
setImageAsBackground();


function toggleModal() {
    const modalBtns = document.querySelectorAll('.modal__btn');
    const clsModal = document.querySelectorAll('.modal__close');
    const modals = document.querySelectorAll('.modal');
    const body = document.querySelector('body');

    modalBtns.forEach(e => e.addEventListener('click', function(){
        const modal = document.querySelector('.' + this.dataset.modal);
        modal.classList.add('active');
        body.classList.add('lock');
        const iframe = modal.querySelector('iframe')
        if(iframe) iframe.src += '&autoplay=1';
    }));

    clsModal.forEach(e => e.addEventListener('click', function(){
        const modal = this.closest('.modal');
        modal.classList.remove('active');
        body.classList.remove('lock');
        const iframe = modal.querySelector('iframe')
        if (iframe) iframe.src = iframe.getAttribute('src').replace('&autoplay=1', '');
    }));

    window.addEventListener('click', function(e){
        if(e.target.classList.contains('modal__window')){
            modals.forEach(modal => {
                modal.classList.remove('active');
                const iframe = modal.querySelector('iframe')
                if (iframe) iframe.src = iframe.getAttribute('src').replace('&autoplay=1', '');
            });
            body.classList.remove('lock');
        }
    });
}
toggleModal();

function initSlider() {
    const opts = {
        slidesPerView: 3,
        slidesPerGroup:1,
        centeredSlides: true,
        loop:true,
        navigation: {
            nextEl: ".review__btn-next",
            prevEl: ".review__btn-prev",
        },
        on: {
            init: function (e) {
                const existingSlides = this.slides;
                let slidesTotal = existingSlides.length;
                const SPV = this.passedParams.slidesPerView;
                while(slidesTotal <= SPV * 2){
                    existingSlides.forEach((e, indx) => {
                        this.addSlide(slidesTotal++,
                            `<div class="review__slide swiper-slide">
                                ${e.innerHTML}
                            </div>`);
                    });
                }
            }
        }
    }
    new Swiper('.review__slider', opts);
}
initSlider();


function showNavBlock() {
    const mm = gsap.matchMedia();
    const tl = gsap.timeline({defaults: {ease: 'power2.in'}})
    mm.add('(min-width: 768px)', () => {
        tl.from('.nav',{opacity: 0, y:-100, duration: .6, delay:.5})
    });
    

}

showNavBlock();

function showHeroBlock() {
    const mm = gsap.matchMedia();
    const tl = gsap.timeline({
        scrollTrigger: {
            trigger: '.hero__row',
            start: 'top top+=150px',
            toggleActions: 'play none complete none',
        },
        defaults: {ease: 'power2.in'}
    });
    mm.add('(min-width:992px)', () => {
       tl.from('.hero__img-block', {opacity:0 , duration:.6, delay:.4})
           .from('.hero__title', {opacity: 0, y:-100,duration:1.2})
           .from('.hero__desc',{opacity: 0, y: 80, duration:.9})
           .from('.hero__link', {opacity:0, y: 80, duration:.9})
           .from('.quote', {opacity: 0, y:100, duration:.6})
           .from('.hero__form-wrapper', {opacity:0, y: 100, duration:.9})
    });
    mm.add('(min-width:768px) and (max-width: 991.98px)', () => {
        tl.from('.hero__img-block', {opacity:0 , duration:.6, delay:.4})
            .from('.hero__title', {opacity: 0, y:-70,duration:1})
            .from('.hero__desc',{opacity: 0, y: 60, duration:.7})
            .from('.hero__link', {opacity:0, y: 60, duration:.7})
            .from('.quote', {opacity: 0, y:70, duration:.45})
            .from('.hero__form-wrapper', {opacity:0, y: 70, duration:.75})
    })
    mm.add('(min-width:576px) and (max-width: 767.98px)', () => {
        tl.from('.hero__img-block', {opacity:0 , duration:.6, delay:.4})
            .from('.hero__title', {opacity: 0, y:-60,duration: .9})
            .from('.hero__desc',{opacity: 0, y: 50, duration:.6})
            .from('.hero__link', {opacity:0, y: 50, duration:.6})
            .from('.quote', {opacity: 0, y:- 30, duration:.35})
            .from('.hero__form-wrapper', {opacity:0, y: 30, duration:.45})
    })

    mm.add('(min-width:320px) and (max-width: 575.98px)', () => {
        tl.from('.hero__title', {opacity: 0, y:-40,duration: .75})
            .from('.hero__desc',{opacity: 0, y: 30, duration:.5})
            .from('.hero__link', {opacity:0, y: 30, duration:.5})
            .from('.hero__img-block', {opacity:0 , duration:.4})
            .from('.quote', {opacity: 0, y:- 30, duration:.35})
            .from('.hero__form-wrapper', {opacity:0, duration:.55})
    })
}

showHeroBlock();

function showClientsBlock() {
    const mm = gsap.matchMedia();
    const tl = gsap.timeline({
        scrollTrigger: {
            trigger: '.clients__row',
            start: 'top 60%',
            toggleActions: 'play none complete none',
        },
        defaults: {ease: 'power2.in'}
    });
    mm.add('(min-width: 992px)', () => {
        tl.fromTo('.clients__item',
            {opacity:0, x: 60},
            {opacity:1, x:0, duration:.7, stagger: .4}
        )
    });
    mm.add('(min-width:768px) and (max-width:991.98px)', () => {
        tl.fromTo('.clients__item',
            {opacity: 0, x: 45},
            {opacity: 1, x: 0, duration: .7, stagger: .4}
        )
    })
    mm.add('(min-width:320px) and (max-width:767.98px)', () => {
        tl.fromTo('.clients__item',
            {opacity:0, x: 30},
            {opacity:1, x:0, duration:.7, stagger: .4}
        )
    })
}

showClientsBlock()

function showExperienceBlock() {
    const mm = gsap.matchMedia();
    const tl = gsap.timeline({
        scrollTrigger: {
            trigger: '.experience__row',
            start: 'top 40%',
            toggleActions: 'play none complete none',
        },
        defaults: {ease: 'power2.in'}
    })
    mm.add('(min-width: 992px)', () => {
        tl.from('.experience__title', {opacity:0, y:-100, duration:1})
            .from('.experience__desc', {opacity:0, y: 80, duration:.5})
            .from('.stat-item__desc', {opacity:0, y:20, duration:.3, stagger:.2 })
            .from('.stat-item__value', {opacity:0, y:-20, duration:.3, stagger:.2})
            .fromTo('.experience__btn', {opacity:0, x: 40},{opacity:1, x:0, duration:.3})
            .from('.experience__img-outer', {opacity:0, duration:.4})
            .from('.chart__wrapper', {opacity:0, x: -80, duration:.8})
    });
    mm.add('(min-width: 768px) and (max-width: 991.98px)', () => {
        tl.from('.experience__title', {opacity:0, y:-100, duration:1})
            .from('.experience__desc', {opacity:0, y: 80, duration:.5})
            .from('.stat-item__desc', {opacity:0, y:20, duration:.3, stagger:.2 })
            .from('.stat-item__value', {opacity:0, y:-20, duration:.3, stagger:.2})
            .fromTo('.experience__btn', {opacity:0, x: 40},{opacity:1, x:0, duration:.3})
            .from('.experience__img-outer', {opacity:0, duration:.4})
            .from('.chart__wrapper', {opacity:0, y:-80, duration:.8})
    });
    mm.add('(min-width: 320px) and (max-width: 767.98px)', () => {
        tl.from('.experience__title', {opacity:0, y:-50, duration:.7})
            .from('.experience__desc', {opacity:0, y: 40, duration:.4})
            .from('.stat-item__desc', {opacity:0, y:15, duration:.25, stagger:.15 })
            .from('.stat-item__value', {opacity:0, y:-15, duration:.25, stagger:.15})
            .fromTo('.experience__btn', {opacity:0},{opacity:1, duration:.25})
            .from('.experience__img-outer', {opacity:0, duration:.4})
    });
}

showExperienceBlock();

function showCatalogBlock() {
    const mm = gsap.matchMedia()
    const tl = gsap.timeline({
        scrollTrigger: {
            trigger: '.catalog__row',
            start: 'top 40%',
            toggleActions: 'play none complete none',
        },
        defaults: {ease: 'power2.in'}
    });
    mm.add('(min-width:992px)', () => {
        tl.from('.catalog__title', {opacity:0, y:-100, duration:.9})
            .from('.catalog__desc', {opacity:0, y: 80, duration:.7})
            .from('.catalog__card', {opacity:0, y: -60, duration:.5 , stagger: .4})
    });
    mm.add('(min-width: 768px) and (max-width:991.98px)', () => {
        tl.from('.catalog__title', {opacity:0, y:-80, duration:.7})
            .from('.catalog__desc', {opacity:0, y: 60, duration:.5})
            .from('.catalog__card', {opacity:0, y: -45, duration:.4 , stagger: .35})
    });
    mm.add('(min-width: 576px) and (max-width:767.98px)', () => {
        tl.from('.catalog__title', {opacity: 0, y: -70, duration:.65})
            .from('.catalog__desc', {opacity: 0, y: 40, duration:.4})
        const items = gsap.utils.toArray('.catalog__card')
        items.forEach(item => {
            gsap.from(item, {
                scrollTrigger:{
                    trigger: item,
                    start: 'top 50%',
                    toggleActions: 'play none complete none',
                },
                opacity:0,
                x:40,
                duration:.3,
                ease: 'power2.in',
            })
        })
    });

    mm.add('(min-width: 320px) and (max-width:575.98px)', () => {
        tl.from('.catalog__title', {opacity:0, y:-50, duration:.4})
            .from('.catalog__desc', {opacity:0, y: 35, duration:.35})
        const items = gsap.utils.toArray('.catalog__card')
        items.forEach(item => {
            gsap.from(item, {
                scrollTrigger:{
                    trigger: item,
                    start: 'top 40%',
                    toggleActions: 'play none complete none',
                },
                opacity:0,
                y:20,
                duration: .5,
                ease: 'power2.in'
            })
        });
    });


}
showCatalogBlock();

function showPropertyBlock() {
    const mm = gsap.matchMedia()
    const tl = gsap.timeline({
        scrollTrigger: {
            trigger: '.property__row',
            start: 'top 50%',
            toggleActions: 'play none complete none',
        },
        defaults: {ease: 'power2.in'}
    });
    mm.add('(min-width:992px)', () => {
        tl.from('.property__video', {opacity:0, duration:.6})
            .from('.property__title', { opacity: 0, y: -80, duration: .55})
            .from('.property__desc', { opacity: 0, y: 60, duration: .45})
            .from('.property__address', { opacity: 0, duration: .5})
            .from('.property__price-outer', { opacity: 0, x: 60, duration: .6 })
            .from('.property__rooms-info > .room-info__block', { opacity: 0, y: 20, duration: .3, stagger: .25})
            .from('.gallery__img-wrapper', { opacity: 0, y: -30, duration: .4, stagger: .3})
    })

    mm.add('(min-width:768px) and (max-width: 991.98px)', () => {
        tl.from('.property__video', {opacity:0, duration:.6})
            .from('.property__modal-btn', {opacity:0,  duration:.3})
            .from('.property__title', { opacity: 0, y: -50, duration: .45}, '<')
            .from('.property__desc', { opacity: 0, y: 40, duration: .35})
            .from('.property__address', { opacity: 0, duration: .5})
            .from('.property__price-outer', { opacity: 0, x: 50, duration: .5 })
            .from('.property__rooms-info > .room-info__block', { opacity: 0, x: 30, duration: .3, stagger: .25})
            .from('.gallery__img-wrapper', { opacity: 0, y: -30, duration: .4, stagger: .3})
    })
    mm.add('(min-width:576px) and (max-width: 767.98px)', () => {
        tl.from('.property__title', { opacity: 0, y: -40, duration: .4}, '<')
            .from('.property__desc', { opacity: 0, y: 30, duration: .3})
            .from('.property__address', { opacity: 0, duration: .5})
            .from('.property__price-outer', { opacity: 0, x: 40, duration: .45 })
            .from('.property__rooms-info > .room-info__block', { opacity: 0, x: 25, duration: .25, stagger: .2})
            .from('.property__video', {opacity:0, duration:.6})
            .from('.property__modal-btn', {opacity:0,  duration:.3})
            .from('.gallery__img-wrapper', { opacity: 0, y: -25, duration: .35, stagger: .25})
    })
    mm.add('(min-width:320px) and (max-width: 575.98px)', () => {
        tl.from('.property__title', { opacity: 0, y: -30, duration: .3}, '<')
            .from('.property__desc', { opacity: 0, y: 25, duration: .3})
            .from('.property__address', { opacity: 0, duration: .5})
            .from('.property__price-outer', { opacity: 0, x: 35, duration: .4 })
            .from('.property__rooms-info > .room-info__block', { opacity: 0, x: 2, duration: .2, stagger: .15})
            .from('.property__video', {opacity:0, duration:.5})
            .from('.property__modal-btn', {opacity:0,  duration:.25})
            .from('.gallery__img-wrapper', { opacity: 0, y: -25, duration: .3, stagger: .2})
    })
}
showPropertyBlock()

function showActivityBlock() {
    const mm = gsap.matchMedia()
    const tl = gsap.timeline({
        scrollTrigger: {
            trigger: '.activity__row',
            start: 'top 40%',
            toggleActions: 'play none complete none',
        },
        defaults: {ease: 'power2.in'}
    });
    mm.add('(min-width:992px)', () => {
        tl.from('.activity__img-wrapper', {opacity: 0, duration: .6})
            .from('.activity__stats', {opacity: 0,  x:40, duration: .5})
            .from('.activity__title', {opacity:0, y:80, duration: .7})
            .from('.activity__desc', {opacity:0, y:-60, duration: .6})
            .from('.activity__list > .list__item', {opacity:0, x:45, duration:.6, stagger:.3})
    });
    mm.add('(min-width: 768px) and (max-width: 991.98px)', () => {
        tl.from('.activity__img-wrapper', {opacity: 0, duration: .6})
            .from('.activity__stats', {opacity: 0,  y:40, duration: .8})
            .from('.activity__title', {opacity:0, y:60, duration: .6})
            .from('.activity__desc', {opacity:0, y:-40, duration: .5})
            .from('.activity__list > .list__item', {opacity:0, x:40, duration:.5, stagger:.25})
    });
    mm.add('(min-width: 576px) and (max-width: 767.98px)', () => {
        const items = gsap.utils.toArray('.activity__list > .list__item')
        const imgTl = gsap.timeline({
            scrollTrigger:{
                trigger: '.activity__img-wrapper',
                start: 'top 50%',
                toggleActions: 'play none complete none',
            },
            defaults: {ease: 'power2.in'}
        });
        tl.from('.activity__title', {opacity:0, y:60, duration: .6})
            .from('.activity__desc', {opacity:0, y:-40, duration: .5})

        imgTl.from('.activity__img-wrapper', {opacity:0, duration: .4})
            .from('.activity__stats', {opacity:0, y:-40, duration: .5})

        items.forEach(item => {
            gsap.from(item, {
                scrollTrigger:{
                    trigger: item,
                    start: 'top 50%',
                    toggleActions: 'play none complete none',
                },
                opacity: 0,
                y:-30,
                duration:.4,
                ease: 'power2.in'
            })
        });
    });

    mm.add('(min-width: 320px) and (max-width: 576.98px)', () => {
        const items = gsap.utils.toArray('.activity__list > .list__item')
        const imgTl = gsap.timeline({
            scrollTrigger:{
                trigger: '.activity__img-wrapper',
                start: 'top 50%',
                toggleActions: 'play none complete none',
            },
            defaults: {ease: 'power2.in'}
        });
        tl.from('.activity__title', {opacity:0, y: 60, duration: .6})
            .from('.activity__desc', {opacity:0, y: -40, duration: .5})

        imgTl.from('.activity__img-wrapper', {opacity:0, duration: .4})
            .from('.activity__stats', {opacity:0, x:40, duration: .8})

        items.forEach(item => {
            gsap.from(item, {
                scrollTrigger:{
                    trigger: item,
                    start: 'top 50%',
                    toggleActions: 'play none complete none',

                },
                opacity: 0,
                y:-25,
                duration: .4,
                ease: 'power2.in'
            })
        });
    });

}

showActivityBlock()

function showReviewBlock(){
    const mm = gsap.matchMedia()
    const tl = gsap.timeline({
        scrollTrigger: {
            trigger: '.review__row',
            start: 'top 40%',
            toggleActions: 'play none complete none',
        },
        defaults: {ease: 'power2.in'}
    });
    mm.add('(min-width: 992px)', () => {
        tl.from('.review__title', {opacity:0, y:-70, duration:.8})
            .from('.review__desc', {opacity:0, y:50, duration:.6})
    });
    mm.add('(min-width:768px) and (max-width:991.98px)', () => {
        tl.from('.review__title', {opacity:0, y:-65, duration:.6})
            .from('.review__desc', {opacity:0, y:45, duration:.5})
    });
    mm.add('(min-width:576px) and (max-width:767.98px)', () => {
        tl.from('.review__title', {opacity:0, y:-60, duration:.55})
            .from('.review__desc', {opacity:0, y:40, duration:.45})
    });
    mm.add('(min-width:320px) and (max-width:767.98px)', () => {
        tl.from('.review__title', {opacity:0, y:-35, duration:.4})
            .from('.review__desc', {opacity:0, y:30, duration:.35})
    });
    gsap.from('.review__slider', {
        scrollTrigger: {
            trigger: '.review__slider > .swiper-wrapper',
            start: 'top 50%',
            toggleActions: 'play none complete none',
        },
        opacity: 0,
        duration: .8,
    });
}

showReviewBlock();

