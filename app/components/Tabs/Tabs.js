window.onload = () => {
    document.querySelector(".tabs__link").classList.add('active');
    document.querySelector(".tabs__content").style.display = 'block';
}


function openTab(ctx, tab) {
    const tabContent = document.getElementsByClassName("tabs__content");
    [...tabContent].forEach(e => e.style.display = "none");

    const tabLinks = document.getElementsByClassName("tabs__link");
    [...tabLinks].forEach(e => e.classList.remove('active'));

    document.getElementById(tab).style.display = "block";
    ctx.classList.add('active');
}