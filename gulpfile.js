const app = 'app/',
    dist = 'dist/';

const {src, dest, watch, series, parallel } = require('gulp');

const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const del = require('del');
//const concat = require('gulp-concat');
const htmlmin = require('gulp-htmlmin');
const cleanCss = require('gulp-clean-css');
const newer = require('gulp-newer');
const imagemin = require('gulp-imagemin');
const fileInclude = require('gulp-file-include');
const rename = require('gulp-rename');
const sass = require('gulp-sass')(require('sass'));
const size = require('gulp-size');
const sourcemap = require('gulp-sourcemaps');
const fileinclude = require("gulp-file-include");
const uglify = require('gulp-uglify-es').default;
const mode = require('gulp-mode')({ default: "development",});

const config = {
    app:{
        //**/!(_|parts)*.html
        html: app + '!(_)*.html',
        style: app + 'scss/style.scss',
        js: app + 'js/scripts.js',
        fonts: app + 'fonts/**/*.*',
        img: app + 'img/**/*.+(png|jpg|jpeg|ico|svg|webp)',
    },
    dist:{
        html: dist,
        style: dist + 'css/',
        js: dist + 'js/',
        fonts: dist + 'fonts/',
        img: dist + 'img/',
    },
    watch:{
        html: app + '**/*.html',
        style: app + 'scss/**/*.scss',
        js: app + 'js/**/*.js',
        fonts: app + 'fonts/**/*.*',
        img: app + 'img/**/*.+(png|jpg|jpeg|ico|svg|webp)',
    }
};


const html = () => {
    return src(config.app.html)
        .pipe(fileInclude({ prefix: '@@', path: '@file'}))
        .pipe(mode.production(htmlmin({ /*collapseWhitespace: false,*/ removeComments: true, })))
        .pipe(size())
        .pipe(dest(config.dist.html))
        .pipe(browserSync.stream())
};

const styles = () => {
    return src(config.app.style)
        .pipe(mode.development(sourcemap.init()))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({overrideBrowserslist: ['last 5 versions'], cascade: false}))
        .pipe(rename({suffix: '.min'}))
        .pipe(mode.production(cleanCss()))
        .pipe(mode.development(sourcemap.write(".")))
        .pipe(size())
        .pipe(dest(config.dist.style))
        .pipe(browserSync.stream())
}

const scripts = () => {
    return src(config.app.js)
        .pipe(fileinclude({ prefix: '@@', path: '@file'}))
        .pipe(mode.development(sourcemap.init()))
        .pipe(mode.production(uglify()))
        .pipe(rename({suffix: '.min'}))
        .pipe(mode.development(sourcemap.write(".")))
        .pipe(size())
        .pipe(dest(config.dist.js))
        .pipe(browserSync.stream())
    //.pipe(concat('app.min.js'))
}

const img = () => {
    return src(config.app.img)
        .pipe(newer(config.dist.img))
        .pipe(
            imagemin([
                imagemin.gifsicle({ interlaced: true }),
                imagemin.mozjpeg({ quality: 75, progressive: true }),
                imagemin.optipng({ optimizationLevel: 5 }),
                imagemin.svgo({
                    plugins: [{ removeViewBox: true }, { cleanupIDs: false }],
                }),
            ]),
        )
        .pipe(size())
        .pipe(dest(config.dist.img))
        .pipe(browserSync.stream())
}

const fonts = () => {
    return src(config.app.fonts)
        .pipe(dest(config.dist.fonts))
        .pipe(browserSync.stream())
}

const clear = () => {
    return del([dist + '**/*', '!' + dist + 'img']);
}

const clearAll = () => {
    return del(dist);
}

const watching = () => {
    browserSync.init({
        server: {
            baseDir: dist
        }
    });
    watch(config.watch.style, styles);
    watch(config.watch.html).on('change', browserSync.reload);
    watch(config.watch.js, scripts);
    watch(config.watch.html, html);
    watch(config.watch.img, img);
    watch(config.watch.fonts, fonts);
}

const build = series(clear, parallel(html, styles, scripts, img, fonts));

exports.html = html;
exports.clear = clear;
exports.clear_a = clearAll;
exports.default = series(build, watching);